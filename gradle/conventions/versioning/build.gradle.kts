plugins {
	`kotlin-dsl`
}

group = "opensavvy.decouple"

dependencies {
	implementation(libs.gradle.gitVersion)
}
